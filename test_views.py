import psycopg2
import pytest
from werkzeug.security import generate_password_hash

from app import get_app
from models import User
from utils import get_statements, create_tables

USER_LOGIN = 'user1'
USER_PASSWORD = 'password'


@pytest.fixture(autouse=True)
async def purge_db_fixture(app):
    if 'dbname=test_' not in app['engine'].dsn:
        raise Exception
    async with app['engine'].acquire() as conn:
        for s in get_statements('drop_all'):
            try:
                await conn.execute(s)
            except psycopg2.errors.UndefinedTable:
                pass

        await create_tables(conn)


@pytest.fixture
def make_user(app):
    async def _make_user():
        async with app['engine'].acquire() as conn:
            await conn.execute(
                User.__table__.insert().values(login=USER_LOGIN, password_hash=generate_password_hash(USER_PASSWORD)))
    return _make_user


@pytest.fixture
async def user(make_user):
    return await make_user()


@pytest.fixture
async def app():
    return await get_app('test')


@pytest.fixture
async def anon_client(app, aiohttp_client):
    return await aiohttp_client(app)


@pytest.fixture
async def client(user, anon_client):
    resp = await anon_client.post('/login', data={'login': USER_LOGIN, 'password': USER_PASSWORD})
    assert resp.status == 200
    json = await resp.json()
    api_key = json['data']

    class Client(object):
        def __getattr__(self, item):
            if item in ['get', 'post']:
                async def wrapper(*args, **kwargs):
                    kwargs['headers'] = {'Authorization': f'Bearer {api_key}'}
                    return await getattr(anon_client, item)(*args, **kwargs)
                return wrapper
            return getattr(anon_client, item)

    return Client()


async def test_login(app, make_user, anon_client):
    resp = await anon_client.post('/login')
    assert resp.status == 200
    json = await resp.json()
    assert json['error_code'] == 'validation_error'
    assert 'login' in json['data']
    assert 'password' in json['data']

    resp = await anon_client.post('/login',  data={'login': 'user1', 'password': 'password'})
    assert resp.status == 200
    json = await resp.json()
    assert json['error_code'] == 'invalid_login_or_password'

    await make_user()

    resp = await anon_client.post('/login', data={'login': 'INVALIDLOGIN', 'password': 'password'})
    assert resp.status == 200
    json = await resp.json()
    assert json['error_code'] == 'invalid_login_or_password'

    resp = await anon_client.post('/login', data={'login': 'user1', 'password': 'INVALIDPASSWORD'})
    assert resp.status == 200
    json = await resp.json()
    assert json['error_code'] == 'invalid_login_or_password'

    resp = await anon_client.post('/login', data={'login': 'user1', 'password': 'password'})
    assert resp.status == 200
    json = await resp.json()
    assert isinstance(json['data'], str)


async def test_add(anon_client, client):
    resp = await anon_client.post('/add')
    assert resp.status == 200
    json = await resp.json()
    assert json['error_code'] == 'auth_required'

    resp = await client.post('/add')
    assert resp.status == 200
    json = await resp.json()
    assert json['error_code'] == 'validation_error'
    assert 'text' in json['data']

    resp = await client.post('/add', data={'text': 'some text'})
    assert resp.status == 200
    json = await resp.json()
    assert isinstance(json['data']['id'], int)

    resp = await client.post('/add', data={'text': 'some text', 'parent_id': json['data']['id']})
    assert resp.status == 200
    json = await resp.json()
    assert isinstance(json['data']['id'], int)

    resp = await client.post('/add', data={'text': 'some text', 'parent_id': json['data']['id'] + 1})
    assert resp.status == 200
    json = await resp.json()
    assert json['error_code'] == 'no_such_parent'


async def test_search_by_id(anon_client, client):
    resp = await anon_client.get('/search_by_id')
    assert resp.status == 200
    json = await resp.json()
    assert json['error_code'] == 'auth_required'

    resp = await client.get('/search_by_id')
    assert resp.status == 200
    json = await resp.json()
    assert json['error_code'] == 'validation_error'
    assert 'id' in json['data']

    resp = await client.post('/add', data={'text': 'some text'})
    json_1 = await resp.json()
    id_1 = json_1['data']['id']

    resp = await client.post('/add', data={'text': 'some text', 'parent_id': id_1})
    json_2 = await resp.json()
    id_2 = json_2['data']['id']

    resp = await client.get('/search_by_id', params={'id': id_1})
    assert resp.status == 200
    json = await resp.json()
    assert json == {'data': [{'id': id_2, 'text': 'some text', 'parent_ids': [id_1]}]}

    resp = await client.get('/search_by_id', params={'id': id_2 + 1})
    assert resp.status == 200
    json = await resp.json()
    assert json['error_code'] == 'no_such_node'


async def test_search_by_text(anon_client, client):
    resp = await anon_client.get('/search_by_text')
    assert resp.status == 200
    json = await resp.json()
    assert json['error_code'] == 'auth_required'

    resp = await client.get('/search_by_text')
    assert resp.status == 200
    json = await resp.json()
    assert json['error_code'] == 'validation_error'
    assert 'text' in json['data']

    resp = await client.post('/add', data={'text': 'cat'})
    json = await resp.json()
    _id_1 = json['data']['id']

    await client.post('/add', data={'text': 'dog', 'parent_id': _id_1})

    resp = await client.get('/search_by_text', params={'text': 'cats'})
    assert resp.status == 200
    json = await resp.json()
    assert json == {'data': [{'id': _id_1, 'text': 'cat', 'parent_ids': []}]}

import aiohttp_session

from models import Session


class PostgreSQLStorage(aiohttp_session.AbstractStorage):
    async def load_session(self, request):
        auth_header = request.headers.get('Authorization')
        if auth_header is None:
            return aiohttp_session.Session(None, data=None, new=True, max_age=self.max_age)
        try:
            auth_type, api_key = auth_header.split(' ')
        except ValueError:
            return aiohttp_session.Session(None, data=None, new=True, max_age=self.max_age)
        if auth_type != 'Bearer':
            return aiohttp_session.Session(None, data=None, new=True, max_age=self.max_age)
        async with request.app['engine'].acquire() as conn:
            query = Session.__table__.select().where(Session.__table__.c.id == api_key)
            res = await conn.execute(query)
            s = await res.fetchone()
            if s is None:
                return aiohttp_session.Session(None, data=None, new=True, max_age=self.max_age)
            else:
                return aiohttp_session.Session(s.id, data=s.data, new=False, max_age=self.max_age)

    async def save_session(self, request, response, session):
        async with request.app['engine'].acquire() as conn:
            await conn.execute(
                Session.__table__.insert().values(id=session.identity, data=session._mapping))
from functools import wraps

import aiohttp_session
from aiohttp import web


def auth(func):
    @wraps(func)
    async def wrapper(request):
        s = await aiohttp_session.get_session(request)
        if s.new:
            return web.json_response({
                'error_code': 'auth_required'
            })
        else:
            pass

        return await func(request)
    return wrapper
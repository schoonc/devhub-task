import asyncio

from aiohttp import web
from aiohttp_swagger import setup_swagger

from app import get_app
from utils import create_tables

loop = asyncio.get_event_loop()
app = loop.run_until_complete(get_app('default'))
loop.run_until_complete(create_tables(app['engine']))
setup_swagger(app)

web.run_app(app, host="127.0.0.1")

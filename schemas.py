import marshmallow as mm

from constants import MIN_LOGIN_LEN, MAX_LOGIN_LEN, MIN_PASSWORD_LEN, MAX_PASSWORD_LEN


class AddSchema(mm.Schema):
    parent_id = mm.fields.Integer()
    text = mm.fields.String(required=True)


class LoginSchema(mm.Schema):
    login = mm.fields.String(validate=mm.validate.Length(min=MIN_LOGIN_LEN, max=MAX_LOGIN_LEN), required=True)
    password = mm.fields.String(validate=mm.validate.Length(min=MIN_PASSWORD_LEN, max=MAX_PASSWORD_LEN), required=True)


class SearchByIdSchema(mm.Schema):
    id = mm.fields.Integer(required=True)


class SearchByTextSchema(mm.Schema):
    text = mm.fields.String(required=True)

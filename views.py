import uuid

import aiohttp_session
from aiohttp import web
from marshmallow import ValidationError
from sqlalchemy import func
from sqlalchemy_utils import Ltree
from werkzeug.security import check_password_hash, generate_password_hash

from decorators import auth
from models import User, Node, id_seq
from schemas import LoginSchema, AddSchema, SearchByIdSchema, SearchByTextSchema


@auth
async def add(request):
    """
    ---
    description: Добавляет элемент в дерево.
    """
    raw_post_data = await request.post()
    try:
        post_data = AddSchema().load(raw_post_data)
    except ValidationError as e:
        return web.json_response({
            'error_code': 'validation_error',
            'data': e.normalized_messages()
        })
    async with request.app['engine'].acquire() as conn:
        parent = None
        if 'parent_id' in post_data:
            query = Node.__table__.select().where(Node.__table__.c.id == post_data['parent_id'])
            res = await conn.execute(query)
            parent = await res.fetchone()
            if parent is None:
                return web.json_response({
                    'error_code': 'no_such_parent'
                })
        query = await conn.execute(id_seq.next_value().select())
        _id = await query.scalar()
        ltree_id = Ltree(str(_id))
        query = Node.__table__.insert().values(id=_id, text=post_data['text'], path=ltree_id if parent is None else parent.path + ltree_id)
        await conn.execute(query)

        return web.json_response({
            'data': {
                'id': _id
            }
        })


@auth
async def search_by_text(request):
    """
    ---
    description: Выполняет полнотекстовый поиск элемента с заданным текстом.
    """
    try:
        query_data = SearchByTextSchema().load(request.query)
    except ValidationError as e:
        return web.json_response({
            'error_code': 'validation_error',
            'data': e.normalized_messages()
        })
    async with request.app['engine'].acquire() as conn:
        query = Node.__table__.select().where(func.to_tsvector('english', Node.__table__.c.text).match(query_data['text']))
        res = await conn.execute(query)
        nodes = await res.fetchall()
        nodes = map(lambda node: {'id': node.id, 'text': node.text,
                                  'parent_ids': [int(bit) for bit in str(node.path).split('.')][:-1]}, nodes)
        return web.json_response({
            'data': list(nodes)
        })


@auth
async def search_by_id(request):
    """
    ---
    description: Выполняет поиск элемента с заданным идентификатором.
    """
    try:
        query_data = SearchByIdSchema().load(request.query)
    except ValidationError as e:
        return web.json_response({
            'error_code': 'validation_error',
            'data': e.normalized_messages()
        })
    async with request.app['engine'].acquire() as conn:
        query = Node.__table__.select().where(Node.__table__.c.id == query_data['id'])
        res = await conn.execute(query)
        node = await res.fetchone()
        if node is None:
            return web.json_response({
                'error_code': 'no_such_node'
            })
        query = Node.__table__.select().where(Node.__table__.c.path.descendant_of(node.path))
        res = await conn.execute(query)
        nodes = await res.fetchall()
        nodes = filter(lambda node: node.id != query_data['id'], nodes)
        nodes = map(lambda node: {'id': node.id, 'text': node.text, 'parent_ids': [int(bit) for bit in str(node.path).split('.')][:-1]}, nodes)
        return web.json_response({
            'data': list(nodes)
        })


async def login(request):
    """
    ---
    description: Аутентифицирует пользователя по логину и паролю.
    """
    raw_post_data = await request.post()
    try:
        post_data = LoginSchema().load(raw_post_data)
    except ValidationError as e:
        return web.json_response({
            'error_code': 'validation_error',
            'data': e.normalized_messages()
        })
    async with request.app['engine'].acquire() as conn:
        query = User.__table__.select().where(User.__table__.c.login == post_data['login'])
        res = await conn.execute(query)
        user = await res.fetchone()
        if user is None:
            return web.json_response({
                'error_code': 'invalid_login_or_password'
            })
        if check_password_hash(user.password_hash, post_data['password']):
            s = await aiohttp_session.get_session(request)
            s.set_new_identity(uuid.uuid4().hex)
            s['user_id'] = user.id
            s['hashed_password_hash'] = generate_password_hash(user.password_hash)
            return web.json_response({
                'data': s.identity
            })
        else:
            return web.json_response({
                'error_code': 'invalid_login_or_password'
            })

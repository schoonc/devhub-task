from sqlalchemy import Column, BigInteger, String, Text, Sequence, Integer, Index, func
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils import LtreeType

from constants import MAX_LOGIN_LEN

Base = declarative_base()


id_seq = Sequence('nodes_id_seq')

class User(Base):
    __tablename__ = 'users'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    login = Column(String(MAX_LOGIN_LEN), unique=True, nullable=False)
    password_hash = Column(String, nullable=False)


class Session(Base):
    __tablename__ = 'sessions'

    id = Column(String, primary_key=True)
    data = Column(JSONB, nullable=False)


class Node(Base):
    __tablename__ = 'nodes'

    id = Column(Integer, id_seq, primary_key=True)
    text = Column(Text, nullable=False)
    path = Column(LtreeType, nullable=False)

    __table_args__ = (
        Index('nodes_text_index', func.to_tsvector('english', text), postgresql_using='gin'),
        Index('nodes_path_index', path, postgresql_using='gist'),
    )

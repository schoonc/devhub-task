import aiohttp_session
from aiohttp.web_app import Application
from aiopg.sa import create_engine

import config
from storage import PostgreSQLStorage
from views import login, add, search_by_text, search_by_id


async def get_app(db_type):
    app = Application()
    aiohttp_session.setup(app, PostgreSQLStorage(max_age=config.SESSION_MAX_AGE))
    engine = await create_engine(**config.DB[db_type])
    app['engine'] = engine
    app.router.add_post('/login', login)
    app.router.add_post('/add', add)
    app.router.add_get('/search_by_text', search_by_text)
    app.router.add_get('/search_by_id', search_by_id)

    return app
import psycopg2
from aiopg.sa import Engine
from sqlalchemy import create_engine

from models import Base


async def create_tables(conn):
    if isinstance(conn, Engine):
        conn = await conn.acquire()
    for s in get_statements('create_all'):
        try:
            await conn.execute(s)
        except psycopg2.errors.DuplicateTable:  # Если таблица существует - игнорируем ошибку.
            pass


def get_statements(method):
    statements = []

    def dump(sql, *multiparams, **params):
        statements.append(sql.compile(engine).statement)

    engine = create_engine('postgresql://', strategy='mock', executor=dump)
    getattr(Base.metadata, method)(engine)
    return statements

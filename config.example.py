SESSION_MAX_AGE = 60 * 60
DB = {
    'default': {
        'user': '',
        'password': '',
        'host': '',
        'database': ''
    },
    'test': {
        'user': '',
        'password': '',
        'host': '',
    }
}

DB['test']['database'] = f"test_f{DB['default']['database']}"
